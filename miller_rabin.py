import random
import argparse
import gmpy2
from gmpy2 import mpz,mpq,mpfr,mpc

#code from https://rosettacode.org/wiki/Miller%E2%80%93Rabin_primality_test#Python modified 
#with gmpy and specific changes for my usecase
#inputs: n - the number to test for primality
#        k - the amount of times to test for primality
def miller_rabin_test(n, k = 20, base2 = False):
    n = mpz(n)
    if n == 2 or n == 1:
        return True
    if n % 2 == 0:
        return False
    # write n-1 as 2**s * d
    # repeatedly try to divide n-1 by 2
    s = 0
    d = n-1
    while True:
        quotient, remainder = gmpy2.f_divmod(d, 2)
        if remainder == 1:
            break
        s += 1
        d = quotient

    # test the base a to see whether it is a witness for the compositeness of n
    def try_composite(a):
        if gmpy2.powmod(a, d, n) == 1:
            return False
        for i in range(s):
            if gmpy2.powmod(a, 2**i * d, n) == n-1:
                return False
        return True # n is definitely composite

    #case for use in baillie-PSW test
    if base2:
        if try_composite(2):
            return False
    else:
        for i in range(k):
            a = mpz(random.randrange(2, n - 2))
            if try_composite(a):
                return False
    return True 

#function to check for negative int arguments
def check_positive(value):
    ivalue = int(value)
    if ivalue < 0:
         raise argparse.ArgumentTypeError("%s is an invalid positive int value" % value)
    return ivalue

#what to do if the file is run as a script independently
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description = "Implementation of Miller - Rabin primality test")
    parser.add_argument('n', metavar='n', type=check_positive, help='the number to test for primality')
    parser.add_argument('k', metavar='k', type=check_positive, help='the amount of times to test for primality')
    args = parser.parse_args()
    result = miller_rabin_test(args.n, args.k)
    print(result)