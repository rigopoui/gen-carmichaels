#coding=UTF-8
import random
limit = 8
Lambda = [nth_prime(i) for i in range(1,limit)]

h = 2
for i in range(len(Lambda)):
	if h > 1 and random.randint(0,5) == 3:
		h -= 1
	Lambda[i] = Lambda[i]**h

prod = product(Lambda)
print(prod)

M = []
print(Lambda)
print(divisors(prod))
for a in divisors(prod):
    if is_prime(a+1) and mod(prod, a+1)<>0:
        M.append(a+1)
print M

#px lambda pairs me dunameis[(2,4), (3,4), (5,3), (7,2), (9,1)] <-limit=6
#θέλουμε απαρίθμιση με τη σειρά [2**1, 2**2, 2**3, 2**4, 2**1 * 3**1, 2**1 * 3**2, 2**1, 3**3, 2**1 * 3**4, 2**2 * 3**1, 2**2 * 3**2,....]
#είναι unique το κάθε στοιχείο;;
#στο πχ: [2, 4, 8, 16, 6, 18, 54, 162, 12, 36, 108, 324,...] φαίνεται να είναι
#μέγεθος πίνακα; έστω (qi,hi) τότε: (q1**1 +q1**2...)+(q2**1+...)+...+(qn**1+...) μόνοι τους οι όροι σε κάθε πιθανή δύναμη. μετά για κάθε (qi,hi) πολλαπλασιάζεται με όλα 
#τα υπόλοιπα (qi,hi) -> (q1**1*(q2*1)+q1**1*(q2**2)+...+q1**1(qn**n)+q1**2*(q2**1)+...+q1**n*(qn**n)
