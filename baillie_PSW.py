import random
import argparse
from math import sqrt
try:
    from scripts.miller_rabin import miller_rabin_test
except ModuleNotFoundError:
    from miller_rabin import miller_rabin_test
import gmpy2
from gmpy2 import mpz
#code from http://ideone.com/57Iayq and https://github.com/smllmn/baillie-psw, modified to use gmpy2

#Perform the Lucas probable prime test

# lucas pseudoprimality test
def jacobi(a, m):
    # assumes a an integer and
    # m an odd positive integer
    a = gmpy2.f_mod(a, m)
    t = 1
    while a != 0:
        z = -1 if gmpy2.f_mod(m, 8) in [3,5] else 1
        while a % 2 == 0:
            a, t = a // 2, t * z
        if a%4 == 3 and m%4 == 3: t = -t
        a, m = m % a, a
    return t if m == 1 else 0

def selfridge(n):
    d, s = 5, 1
    while True:
        ds = d * s
        if gmpy2.gcd(ds, n) > 1:
            return ds, 0, 0
        if jacobi(ds, n) == -1:
            return ds, 1, (1 - ds) // 4
        d, s = d + 2, s * -1

def lucasPQ(p, q, m, n):
    # nth element of lucas sequence with
    # parameters p and q (mod m); ignore
    # modulus operation when m is zero
    def mod(x):
        if m == 0: return x
        return gmpy2.f_mod(x, m)
    def half(x):
        if x % 2 == 1: x = x + m
        return mod(gmpy2.f_div(x, 2))
    un, vn, qn = 1, p, q
    u = 0 if gmpy2.f_mod(n,2) == 0 else 1
    v = 2 if gmpy2.f_mod(n,2) == 0 else p
    k = 1 if gmpy2.f_mod(n,2) == 0 else q
    n, d = gmpy2.f_div(n, 2), p * p - 4 * q
    while n > 0:
        u2 = mod(un * vn)
        v2 = mod(vn * vn - 2 * qn)
        q2 = mod(qn * qn)
        n2 = gmpy2.f_div(n,2)
        if gmpy2.f_mod(n,2) == 1:
            uu = half(u * v2 + u2 * v)
            vv = half(v * v2 + d * u * u2)
            u, v, k = uu, vv, k * q2
        un, vn, qn, n = u2, v2, q2, n2
    return u, v, k

def isLucasPseudoprime(n):
    d, p, q = selfridge(n)
    if p == 0:
        return n == d
    u, v, k = lucasPQ(p, q, n, n+1)
    return u == 0

#Return a list of the prime factors for a natural number.
def trial_division(n):
    a = []
    #f = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47]
    f = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 
    59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 
    131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193, 
    197, 199, 211, 223, 227, 229, 233, 239, 241, 251, 257, 263, 269, 
    271, 277, 281, 283, 293, 307, 311, 313, 317, 331, 337, 347, 349, 
    353, 359, 367, 373, 379, 383, 389, 397, 401, 409, 419, 421, 431, 
    433, 439, 443, 449, 457, 461, 463, 467, 479, 487, 491, 499, 503, 
    509, 521, 523, 541]
    for i in f:
        if n == i:
            return "easy"
        elif gmpy2.f_mod(n, i) == 0:        
            return False
    return True

#inputs: n - the number to test for primality
def baillie_psw_test(n):
    n = mpz(n)
    #doing a trial division with small primes
    trial = trial_division(n)
    if trial == "easy":
        return True

    if not trial:
        return False

    #using miller_rabin test as a base 2 strong probable prime test
    if not miller_rabin_test(n, 1, True):
        return False

    # Check that the candidate isn't a square number
    if gmpy2.is_square(n):
        return False

    # Finally perform the Lucas primality test
    if not isLucasPseudoprime(n):
        return False
    return True

#function to check for negative int arguments
def positive_integer(value):
    ivalue = int(value)
    if ivalue < 0:
         raise argparse.ArgumentTypeError("%s is an invalid positive integer value" % value)
    return ivalue

#what to do if the file is run as a script independently
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description = "Implementation of Miller - Rabin primality test")
    parser.add_argument('n', metavar='n', type=positive_integer, help='the number to test for primality')
    args = parser.parse_args()
    result = baillie_psw_test(args.n)
    print(result)