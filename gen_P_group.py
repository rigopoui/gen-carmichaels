from random import randint
import argparse
import math
from baillie_PSW import baillie_psw_test as is_prime
#check if a number is a power of another number
#is n a power of b?
def is_power(n, b):
    return b**int(math.log(n, b)+0.5)==n

#create the highly composite lambda number
#h_start is the first value of h_i, which is non-increasing
#returns a list containing the factors of Lambda
def make_Lambda(h_start, L_length):
    #in sage, use primes function to compute these.
    primes = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 
    59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 
    131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193, 
    197, 199, 211, 223, 227, 229, 233, 239, 241, 251, 257, 263, 269, 
    271, 277, 281, 283, 293, 307, 311, 313, 317, 331, 337, 347, 349, 
    353, 359, 367, 373, 379, 383, 389, 397, 401, 409, 419, 421, 431, 
    433, 439, 443, 449, 457, 461, 463, 467, 479, 487, 491, 499, 503, 
    509, 521, 523, 541]
    h = h_start 
    Lambda = []
    Lambda_prod = 1
    for i in range(0,L_length):
        Lambda_prod *= primes[i]**h
        for j in range(1,h+1):
            Lambda.append(primes[i]**j)
        if h > 1 and randint(1,max(h,L_length)) == h:
            if h>=3 :
                h -= randint(1,h-2)
            else:
                h-= 1
    return Lambda, Lambda_prod
#creates the complete list of divisors, not recommended, lol
#only useful for a small enough L
def make_div_list(L):
    #the length of the divisor list
    limit = 1 << len(L)
    M=[]
    #print(M)
    i = 3
    while i < limit: 
        if is_power(i, 2):
            i += 1
        bin_i = bin(i)[2:][::-1]
        num=1
        for j in range(len(bin_i)):
            if bin_i[j] == '1':
                num *= L[j]
        M.append(num)
        i += 1
    return M

#accepts values from 0 to len(M) - 1, where M is the complete list
#of divisors
#this function finds a specific divisor in log(n) time, without
#having to compute any previous divisors - O(1) space complexity
def find_divisor(n, Lambda):
    n += 3
    n_log = int(math.log(n,2)) - 1
    n += n_log
    bin_n = bin(n)[2:][::-1]
    num = 1
    for i in range(len(bin_n)):
        if bin_n[i] == '1':
            num *= Lambda[i]
    return num
#code to check for repetitions that the find_divisor method causes
#the amount of repetition is proportionate to the ratio of h_start/L_length
"""L = make_Lambda(3, 6)
M_length = (1 << len(L)) - len(L) - 1
M = []
print(M_length)
if M_length < 1000:
    M = make_div_list(L)
else:
    for i in range(0, 1000):
        k = randint(0, M_length - 1)
        M.append(find_divisor(k, L))

k = 0
#check for repeating numbers.
for i in range(len(M)):
    for j in range(i+1, len(M)):
        if M[i] == M[j]:
            #print(i, j,  M[i])
            k+=1
print(k, len(M), k/len(M))"""
#for div in M check if div+1 is a prime and div+1 % L !=0

#creates the group P, a subgroup of the divisors of Lambda, where p in P is a prime and p-1 is not a member of Lambda
def make_P_group(Lambda, Lambda_prod):
    M_length = (1 << len(Lambda)) - len(Lambda) - 1
    P = set()
    for i in range(0, M_length):
        a = find_divisor(i, Lambda)
        p = a+1
        if is_prime(p) and Lambda_prod % p != 0:
            P.add(p)
    return P

def positive_integer(value):
    ivalue = int(value)
    if ivalue < 1:
         raise argparse.ArgumentTypeError("%s is an invalid positive integer value" % value)
    return ivalue

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description = "Create the P set in python - next,  put in Sagemath")
    parser.add_argument('l', metavar='l', type=positive_integer, help = 'The length in number of primes - of the desired Lambda number')
    parser.add_argument('h', metavar='h', type=positive_integer, help='The starting value of the power h')
    args = parser.parse_args()
    L = make_Lambda(args.h, args.l)
    result = make_P_group(L[0], L[1])
    print(result)
    print(len(result))
